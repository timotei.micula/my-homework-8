<?php
 
 require('vehicle.class.php');

 class car extends vehicle {
     private $hp;
     private $fuelType;
     
     public function __construct($hp, $fuelType ){
        $this->hp = $hp;
        $this->fuelType = $fuelType;
    }

    public function setHp($val) {
        $this->hp = $val;
    }

     public function getHp() {
            return $this->hp;
        }

        public function setFuelType($val) {
            $this->fuelType = $val;
        }
        
         public function getFuelType() {
                return $this->fuelType;
            }
       

 }

?>