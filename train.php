<?php

require('transport.class.php');

class plane extends transport{
    private $nrWagons;
    private $nrSeats;

    public function __construct($nrWagons,$nrSeats){
        $this->nrWagons = $nrWagons;
        $this->nrSeats = $nrSeats;
    }

    public function setnrWagons($val) {
        $this->nrWagons = $val;
    }

    public function getnrWagons() {
        return $this->nrWagons;
    }
   
   
    public function setNrSeats($val) {
        $this->nrSeats = $val;
    }

    public function getNrSeats() {
        return $this->nrSeats;
    }

}



?>
