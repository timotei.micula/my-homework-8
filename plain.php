<?php

require('transport.class.php');

class plane extends transport{
    private $name;
    private $nrSeats;

    public function __construct($name,$nrSeats){
        $this->name = $name;
        $this->nrSeats = $nrSeats;
    }

    public function setName$val {
        $this->name = $val;
    }

    public function getName() {
        return $this->name;
    }
   
   
    public function setNrSeats($val) {
        $this->nrSeats = $val;
    }

    public function getNrSeats() {
        return $this->nrSeats;
    }

}



?>
